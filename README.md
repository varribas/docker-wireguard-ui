![](https://images.microbadger.com/badges/version/jarylc/wireguard-ui.svg) ![](https://images.microbadger.com/badges/image/jarylc/wireguard-ui.svg) ![](https://img.shields.io/docker/stars/jarylc/wireguard-ui.svg) ![](https://img.shields.io/docker/pulls/jarylc/wireguard-ui.svg)

Network needs to be on `host` mode to restart the WireGuard network interface on the host when changes are applied.

# Environment variables:
| Environment | Default value |
|--------------|--------------|
| LOGIN_PAGE   | 1            |
| BIND_ADDRESS | 0.0.0.0:5000 |

# Default username & password
- username: admin
- password: admin

# Volumes
- /db - WGUI configuration and data
- /etc/wireguard - to mount with host's `/etc/wireguard` to apply changes

# Deploying
## Terminal
```bash
docker run -d \
    --name wgui \
    -e LOGIN_PAGE=1 \
    -e BIND_ADDRESS=0.0.0.0:5000 \
    --net=host \
    -v /path/data:/db \
    -v /etc/wireguard:/etc/wireguard \
    --restart unless-stopped \
    --privileged \
    jarylc/wireguard-ui
```
## Docker-compose
```yml
wgui:
    image: jarylc/wireguard-ui
    network_mode: host
    privileged: true
    volumes:
        - /path/data:/db
        - /etc/wireguard:/etc/wireguard
    environment:
        - LOGIN_PAGE=1
        - BIND_ADDRESS=0.0.0.0:5000
    restart: unless-stopped
```
